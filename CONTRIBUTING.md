# Welcome to the Contributing Guide
Although the project has completed its first design cycle, we are still open to your ideas and suggestions for improving this product. Please see below what contributions we require before making changes to the project.

## Making changes
Before adding any changes to the project, please upload a pull request. We will need to review your requested changes, before allowing you to pull from the repository.

## Raising Issues
Should you detect any issues/bugs with our design please raise these on GitLab
